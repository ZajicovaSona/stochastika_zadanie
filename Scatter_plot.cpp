#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <cmath>
#include <limits>

#define n 10			//pocet deliacich bodov na casovej osi
#define pocet 1000		//pocet vygenerovanych trajektorii

using namespace std;

typedef struct trajektoria{
	double t[n];
	double x[n];
}TRAJEKTORIA;

vector<double> normal_distribution(void){
	double x1, x2;
	vector<double> v;		
	x1 = rand() * (1.0 / RAND_MAX);
	x2 = rand() * (1.0 / RAND_MAX);
	
	v.push_back( sqrt(-2.0 * log(x1)) * cos(2 * M_PI * x2) );
	v.push_back( sqrt(-2.0 * log(x1)) * sin(2 * M_PI * x2) );
	
	return v;
}

void trajektorie(TRAJEKTORIA *B){
	vector<double> b;
	double sigma;
	
	B->x[0] = 0.0;
	for(int i = 1; i < n; i += 2){
		b = normal_distribution();
		sigma = sqrt( B->t[i] - B->t[i-1] );
		B->x[i] = B->x[i-1] + b[0] * sigma;
		sigma = sqrt( B->t[i+1] - B->t[i] );
		B->x[i+1] = B->x[i] + b[1] * sigma;
	}
}

void initialize_graph(unsigned char **R, unsigned char **G, unsigned char **B, int width, int height )
{
	for(int i = 0; i < height; i++) {
		for(int j = 0; j < width; j++) {
			R[i][j] = 255;		
			G[i][j] = 255;
			B[i][j] = 255;
		}
	}
	for(int j = 0; j < width; j++) {				//os x
		R[height/2][j] = 0; 	
		G[height/2][j] = 0;
		B[height/2][j] = 0;
	}
	for(int i = 0; i < height; i++) {				//os y
		R[i][width/2] = 0; 	
		G[i][width/2] = 0;
		B[i][width/2] = 0;
	}
}

int main(void){
	TRAJEKTORIA T[pocet];
	float dt = 1. ; 									//casovy krok
	int width = 500, height = 500; 					//rozmery obrazka
	int x_0 = round( width/2. ), y_0 = round( height/2. );
	int step = 25;
	bool red = false;
	FILE *image;
	unsigned char **R = new unsigned char *[height];	//red
	unsigned char **G = new unsigned char *[height];	//green
	unsigned char **B = new unsigned char *[height];	//blue
	for (int i = 0 ; i < height ; i++ ) {
		R[i] = new unsigned char[width];
		G[i] = new unsigned char[width];
		B[i] = new unsigned char[width];
	}
	
	cout << "initializing graph \n";
	initialize_graph(R, G, B, width, height);	
	
	cout << "generating trajectories \n";
	srand(time(NULL));
	for(int k = 0; k < pocet; k++){
		for(int i = 0; i < n; i++){
			T[k].t[i] = i * dt;	
		}
		trajektorie(&T[k]);
	}
	
	cout << "graphing trajectories \n";
	for(int k = 0; k < pocet; k++){
//		v case 1 je hodnota mensia ako nula a v case 6 je vacsia ako 1
		int x = x_0 + round( step * T[k].x[1] );
		int y = y_0 + round( -step * T[k].x[6] );
//		cout << "x\t" << x << "\t y\t" << y << "\n";
		for(int j = x-5; j <= x+5; j++) {
			if( T[k].x[1] < 0 && T[k].x[6] > 1 ) {
				R[y][j] = 255; 	
				G[y][j] = 0;
				B[y][j] = 0;
			}
			else {
				R[y][j] = 0; 	
				G[y][j] = 0;
				B[y][j] = 255;
			}
		}
		for(int j = y-5; j <= y+5; j++) {
			if( T[k].x[1] < 0 && T[k].x[6] > 1 ) {
				R[j][x] = 255; 	
				G[j][x] = 0;
				B[j][x] = 0;
			}
			else {
				R[j][x] = 0; 	
				G[j][x] = 0;
				B[j][x] = 255;
			}
		}
	}
	
	cout << "writing file \n";
	image=fopen("scatter_plot.ppm","w");
   if(image==NULL){
   		cout << "ERROR: Cannot open output file";
    	exit(EXIT_FAILURE);
   }
   fprintf(image,"P3\n");										// filetype
   fprintf(image,"%d %d\n",width,height);						// dimensions
   fprintf(image,"255\n");										// Max pixel

   for(int i = 0; i < height; i++){
      for(int j = 0; j < width; j++){
      	fprintf(image,"%d %d %d ",(unsigned char)R[i][j],(unsigned char)G[i][j],(unsigned char)B[i][j]);
      }
   }
   fclose(image);
   
    cout << "done \n";

	return 1;
}
