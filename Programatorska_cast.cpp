#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <cmath>
#include <limits>

#define n 100			//pocet deliacich bodov na casovej osi
#define pocet 100		//pocet vygenerovanych trajektorii

using namespace std;

typedef struct trajektoria{
	double t[n];
	double x[n];
}TRAJEKTORIA;

vector<double> normal_distribution(void){
	double x1, x2;
	vector<double> v;		
	x1 = rand() * (1.0 / RAND_MAX);
	x2 = rand() * (1.0 / RAND_MAX);
	
	v.push_back( sqrt(-2.0 * log(x1)) * cos(2 * M_PI * x2) );
	v.push_back( sqrt(-2.0 * log(x1)) * sin(2 * M_PI * x2) );
	
	return v;
}

void trajektorie(TRAJEKTORIA *B){
	vector<double> b;
	double sigma;
	
	B->x[0] = 0.0;
	for(int i = 1; i < n; i += 2){
		b = normal_distribution();
		sigma = sqrt( B->t[i] - B->t[i-1] );
		B->x[i] = B->x[i-1] + b[0] * sigma;
		sigma = sqrt( B->t[i+1] - B->t[i] );
		B->x[i+1] = B->x[i] + b[1] * sigma;
	}
}

void line_steep(unsigned char **R, unsigned char **G, unsigned char **B, int x1, int y1, int x2, int y2, bool red){
	int tmp_y;
	if (x1 > x2) { 
		line_steep( R, G, B, x2, y2, x1, y1, red); 
		return;
	}
	double y = y1;         
	double m = (double)(y2 - y1) / (x2 - x1);
	for (int i = x1; i <= x2; i++){
		tmp_y = round(y);
		if(red == true) {
			R[tmp_y][i] = 255;
			G[tmp_y][i] = 0;
			B[tmp_y][i] = 0;
		}
		else {
			R[tmp_y][i] = 0;
			G[tmp_y][i] = 0;
			B[tmp_y][i] = 255;
		}
		y += m;
	}
}


void line (unsigned char **R, unsigned char **G, unsigned char **B, int x1, int y1, int x2, int y2, bool red) {
	double y = y1;
	double m = (double)(y2 - y1) / (x2 - x1);
	int tmp_y;

	if (abs(m) <= 1){
		for (int i = x1; i <= x2; i++){
			tmp_y = round(y);
			if(red == true) {
				R[i][tmp_y] = 255;
				G[i][tmp_y] = 0;
				B[i][tmp_y] = 0;
			}
			else {
				R[i][tmp_y] = 0;
				G[i][tmp_y] = 0;
				B[i][tmp_y] = 255;
			}
			y += m;
		}
	}
	else
		line_steep( R, G, B, y1, x1, y2, x2, red);
}

void initialize_graph(unsigned char **R, unsigned char **G, unsigned char **B, int width, int height )
{
	int step_x = round( width/n );
	for(int i = 0; i < height; i++) {
		if(i == height/2)
			for(int j = 0; j < width; j++) {				//os x
				R[height/2][j] = 0; 	
				G[height/2][j] = 0;
				B[height/2][j] = 0;
			}
		else
			for(int j = 0; j < width; j++)
				if(j == step_x) {							//os y
					R[i][j] = 0; 	
					G[i][j] = 0;
					B[i][j] = 0;
				}
				else {										//biele "pozadie"
					R[i][j] = 255;		
					G[i][j] = 255;
					B[i][j] = 255;
				}
	}
}

int main(void){
	TRAJEKTORIA T[pocet];
	float dt = 1. ; 									//casovy krok
	int width = 2000, height = 1000; 					//rozmery obrazka
	int step_x = round( width/n ), step_y = round( height/100 );
	int sum = 0;
	bool red = false;
	FILE *image;
	unsigned char **R = new unsigned char *[height];	//red
	unsigned char **G = new unsigned char *[height];	//green
	unsigned char **B = new unsigned char *[height];	//blue
	for (int i = 0 ; i < height ; i++ ) {
		R[i] = new unsigned char[width];
		G[i] = new unsigned char[width];
		B[i] = new unsigned char[width];
	}
	
	cout << "initializing graph \n";
	initialize_graph(R, G, B, width, height);	
	
	cout << "generating trajectories \n";
	srand(time(NULL));
	for(int k = 0; k < pocet; k++){
		for(int i = 0; i < n; i++){
			T[k].t[i] = i * dt;	
		}
		trajektorie(&T[k]);
	}
	
	cout << "graphing trajectories \n";
	for(int k = 0; k < pocet; k++){
		//v case 1 je hodnota mensia ako nula a v case 6 je vacsia ako 1
		if( T[k].x[1] < 0 && T[k].x[6] > 1 ){
			sum ++
			;red = true;
		}
		for(int i = 0; i < n-1; i++){
			int x1 = step_x + round(T[k].t[i]*step_x);
			int y1 =  height/2 - round(T[k].x[i]*step_y);
			int x2 = step_x + round(T[k].t[i+1]*step_x);
			int y2 =  height/2 - round(T[k].x[i+1]*step_y);
			if (y1 > y2)
				line( R, G, B, y2, x2, y1, x1, red);
			else
				line( R, G, B, y1, x1, y2, x2, red);
		}
		red = false;
	}
	
	cout << "writing file \n";
	image=fopen("random_variable_100.ppm","w");
   if(image==NULL){
   		cout << "ERROR: Cannot open output file";
    	exit(EXIT_FAILURE);
   }
   fprintf(image,"P3\n");										// filetype
   fprintf(image,"%d %d\n",width,height);						// dimensions
   fprintf(image,"255\n");										// Max pixel

   for(int i = 0; i < height; i++){
      for(int j = 0; j < width; j++){
      	fprintf(image,"%d %d %d ",(unsigned char)R[i][j],(unsigned char)G[i][j],(unsigned char)B[i][j]);
      }
   }
   fclose(image);
   
    cout << "done \n";
    
    cout << "Pocetnost studovaneho javu B[1] < 0 && B[6] >1 \n";
    cout << sum << " / " << pocet << " = " << (double)sum/pocet << "\n";

	return 1;
}
